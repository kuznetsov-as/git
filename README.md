# Git

## Базовые команды Git

> Если команды ниже пугают, то стоит сначала зайти на [LearnGitBranching](https://learngitbranching.js.org/?locale=ru_RU) и пройти несколько уроков. Все сразу станет сильно понятнее.
---
Вводим команду клонирования:
``` bash
git clone ...
```
---
Смотрим в какой ветке находимся:
``` bash
git branch
```
---
Создаем новую ветку (настоятельно рекомендуется отрезать новую ветку от main - т.е. перед созданием новой ветки при помощи **git branch** необходимо убедиться, что в данный момент мы находимся в ветке main):
``` bash
git branch newbranch
```
---
Переключаемся на новую ветку:
``` bash
git checkout newbranch
```
---
После этого вносим изменения в проект, когда закончили, можем посмотреть на изменившиеся/добавленные файлы:
``` bash
git status
```
---
Добавляем файлы для последующего коммита (находимся при этом в корне проекта):
``` bash
git add .
```
Вместо "." иногда проще указать конкретные файлы, которые хотим в дальнейшем отправить в удаленный репозиторий.

---
Фиксируем изменения:
``` bash
git commit -m "Комментарий к коммиту"
```
---
Отправляем все зафиксированные изменения с локального репозитория (с вашего компьютера) в удалённый (на гитлаб):
``` bash
git push
```
---
Связать локальную ветку с удаленной и сделать пуш (Требуется вводить при первом пуше в новую ветку, если забыть - не страшно, Git сам подскажет):
``` bash
git push --set-upstream origin newbranch
```
---
> <span style="color:red">ВНИМАНИЕ!</span>
> После этого из интерфеса гитлаба создаем merge request и затем объединям содержимое своей ветки и ветки main. Запушить изменения напрямую в ветку main не получится (стоит запрет), только через создание merge request. Сам merge request используется для валидации изменений (другие участники проекта (Reviewer) могут зайти и поставить метку Approve, что показывает согласие с внесенными изменениями). После слияния своей ветки с main рекомендуется удалять свою ветку. Также в рамках работы над документацией стоит придерживаться правил: один человек - одна ветка, одна ветка - одна задача. Если несколько человек параллельно будут вносить изменения в одну ветку и в одни и те же файлы - рано или поздно это приведет к конфликтам слияния. Также важно уточнить: при работе в своей ветке необходимо регулярно делать вливание изменений из основной версии (ветка main) в свою ветку. В дальнейшем это уменьшает ручную работу при слиянии документов. Чтобы это сделать необходимо перейти в интерфейс GitLab и создать Merge Request (панель слева) на слияние ветки main и вашей ветки. После того как в вашей удаленной ветке появятся изменения из главной ветки main необходимо будет выполнить команду **git pull** в терминале, находясь при этом в своей локальной ветке (что можно проверить с помощью команды **git branch**). Чем чаще будет происходить подобное - тем меньше вероятность того, что в дальнейшем будут происходить какие-либо конфликты слияния.
---